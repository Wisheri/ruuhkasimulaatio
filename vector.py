import math


class Vector2D(object):
	
	def __init__(self, x=0, y=0):
		self.x = float(x)
		self.y = float(y)

	def __add__(self, val):
		return Point( self[0] + val[0], self[1] + val[1] )

	def __sub__(self, val):
		return Point( self[0] - val[0], self[1] - val[1])

	def __iadd__(self, val):
		self.x = val[0] + self.x
		self.y = val[1] + self.y
		return self

	def __isub__(self, val):
		self.x = self.x - val[0]
		self.y = self.y - val[1]
		return self

	def __div__(self, val):
		return Point( self[0] / val, self[1] / val )

	def __mul__(self, val):
		return Point( self[0] * val, self[1] * val )

	def __idiv__(self, val):
		self[0] = self[0] / val
		self[1] = self[1] / val
		return self

	def __imul__(self, val):
		self[0] = self[0] * val
		self[1] = self[1] * val
		return self

	def __getitem__(self, key):
		if (key == 0):
			return self.x
		elif (key == 1):
			return self.y
		else:
			raise Exception("Invalid key to Point")

	def __setitem__(self, key, value):
		if (key == 0):
			self.x = value
		elif (key == 1):
			self.y = value
		else:
			raise Exception("Invalid key to Point")

	def __str__(self):
		return "(" + str(self.x) + "," + str(self.y) + ")"