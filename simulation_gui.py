import sys
import simulation
import random
import time

from PyQt4 import QtGui, QtCore,Qt

class MainWindow(QtGui.QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()

        self.setWindowTitle("Ruuhkasimulaatio")
        self.resize(1024, 768)
        self.setMinimumSize(800, 600)
        self.setMaximumSize(1024, 768)
        self.set_toolbar()
        self.simulation = simulation.Simulation()
        self.simulation_started = False
        self.amount = 40
        self.destination = [450.0, 10.0]
        self.i = 40.0
        self.j = 500
        self.answered = False
        self.text_counter = 0
        self.text_object_container = []
        self.cancelled = False

        self.simulation_timer = QtCore.QElapsedTimer()
        self.end_time = 0

        self.timer = QtCore.QTimer(self)
        QtCore.QObject.connect(self.timer, QtCore.SIGNAL("timeout()"), self.repaint)
        QtCore.QMetaObject.connectSlotsByName(self)
        self.timer.start(16) #repaint every 16 milliseconds

        self.add_view()

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()
        elif e.key() == QtCore.Qt.Key_Space:
            self.start_simulation()
        elif e.key() == QtCore.Qt.Key_Shift:
            self.cancel_simulation()
        elif e.key() == QtCore.Qt.Key_J:
            self.add_player()
        elif e.key() == QtCore.Qt.Key_K:
            self.sub_player()


    def set_toolbar(self):
        start = QtGui.QAction(self.tr("&Start"), self)
        start.setShortcut(QtGui.QKeySequence.New)

        cancel = QtGui.QAction(self.tr("&Cancel"), self)
        cancel.setShortcut(QtGui.QKeySequence.New)

        add = QtGui.QAction(self.tr("&+"), self)
        add.setShortcut(QtGui.QKeySequence.New)

        sub = QtGui.QAction(self.tr("&-"), self)
        sub.setShortcut(QtGui.QKeySequence.New)

        self.main_toolbar = QtGui.QToolBar(self)
        self.addToolBar(self.main_toolbar)
        self.main_toolbar.addAction(start)
        self.main_toolbar.addAction(cancel)
        self.main_toolbar.addAction(add)
        self.main_toolbar.addAction(sub)

        start.triggered.connect(self.start_simulation)
        cancel.triggered.connect(self.cancel_simulation)
        add.triggered.connect(self.add_player)
        sub.triggered.connect(self.sub_player)

    def start_simulation(self):
        if self.simulation_started: #The simulation won't start again if it's already running
            return None
        self.scene.clear()
        self.scene.addRect(0, 0, 420, 100, QtGui.QColor(QtCore.Qt.black), QtGui.QColor(QtCore.Qt.black))
        self.scene.addRect(500, 0, 524, 100, QtGui.QColor(QtCore.Qt.black), QtGui.QColor(QtCore.Qt.black))

        text_object = QtGui.QGraphicsTextItem("Amount: " + str(self.amount))
        text_object.setFont(QtGui.QFont("Times", 30))
        self.scene.addItem(text_object)
        text_object.setX(780.0)
        self.text_object_container = []
        self.text_object_container.append(text_object)

        self.simulation_started = True
        self.cancelled = True
        self.answered = False
        self.text_counter = 0
        if self.end_time == 0: #If this is the first time
            self.simulation_timer.start()
        else:
            self.simulation_timer.restart()

    def add_player(self):
        self.amount += 1
        if len(self.text_object_container) == 0:
            return None
        text_object = self.text_object_container[0]
        text_object.setPlainText("Amount: " + str(self.amount))

    def sub_player(self):
        if self.amount > 0 and self.simulation_started == False: #You can't "destory" player during the simulation, so you can't sub if the simulation is running
            self.amount -= 1
            if len(self.text_object_container) == 0:
                return None
            text_object = self.text_object_container[0]
            text_object.setPlainText("Amount: " + str(self.amount))

    def cancel_simulation(self): #Setting all the necessary boolean values and other to default
        self.scene.clear()
        self.simulation.clear_list()
        self.i = 40.0
        self.j = 500.0
        if self.simulation_started:
            self.end_time = self.simulation_timer.elapsed()
        self.simulation_started = False


        self.scene.addRect(0, 0, 420, 100, QtGui.QColor(QtCore.Qt.black), QtGui.QColor(QtCore.Qt.black))
        self.scene.addRect(500, 0, 524, 100, QtGui.QColor(QtCore.Qt.black), QtGui.QColor(QtCore.Qt.black))

        text_object = QtGui.QGraphicsTextItem("Amount: " + str(self.amount))
        text_object.setFont(QtGui.QFont("Times", 30))
        self.scene.addItem(text_object)
        text_object.setX(780.0)
        self.text_object_container = []
        self.text_object_container.append(text_object)

    def paintEvent(self, event): #The paint method which is called every 16 milliseconds
        qp = QtGui.QPainter()
        qp.begin(self)
        self.drawPeople(event, qp)
        if self.simulation_started == False and self.end_time != 0 and self.answered == False and self.cancelled == False: #Makes sure it won't get repainted
            self.drawText(event, qp)
        qp.end()


    def drawPeople(self, event, qp):
        if self.simulation_started:
            destroy_list = self.simulation.get_destroy_list()
            if len(destroy_list) != 0: #Destroying the objects that have reached their goal
                for i in destroy_list:
                    self.scene.removeItem(i)
                self.simulation.list_destroyed()

            if len(self.text_object_container) == 1: #Drawing the timer on the left corner of the screen
                time_object = QtGui.QGraphicsTextItem(str(float(self.end_time) / 1000))
                time_object.setFont(QtGui.QFont("Times", 30))
                self.scene.addItem(time_object)
                self.text_object_container.append(time_object)
            else:
                time_object = self.text_object_container[1]
                time_object.setPlainText(str(float(self.simulation_timer.elapsed()) / 1000))

            simulation_running = self.simulation.simulation_running()      
            if simulation_running == True: #Returns true if every single object has reached its goal
                self.cancelled = False
                self.cancel_simulation()
                
            people_list = self.simulation.get_people()
            if len(people_list) < self.amount and self.simulation_started: #Draws more objects on the screen if there aren't enough
                rand = random.randint(1, 1024)
                rect = QtGui.QGraphicsRectItem(0.0, 0.0, 10.0, 10.0)
                self.scene.addItem(rect)
                if self.i >= 1200:
                    self.i = 40.0
                    self.j += 40.0
                rect.setPos(self.i, self.j)
                self.i += 80.0
                self.simulation.set_person(rect)

            self.simulation.move(self.destination)


    def drawText(self, event, qp): #After a successful simulation
        if self.text_counter == 0:
            self.answered = True
            self.scene.addText(str(float(self.end_time) / 1000) + " seconds", QtGui.QFont("Times", 30))
            self.text_counter += 1

            msg_box = QtGui.QMessageBox()
            msg_box.setText("Haluatko tallentaa?")
            msg_box.setWindowTitle("Ruuhkasimulaatio")
            msg_box.addButton(QtGui.QMessageBox.No)
            msg_box.addButton(QtGui.QMessageBox.Save)
            ret = msg_box.exec_()
            if ret == QtGui.QMessageBox.Save:
                self.simulation.save_log(self.amount, self.end_time)
            else:
                pass


    def add_view(self):
        self.scene = QtGui.QGraphicsScene()
        self.view = MyView()
        self.view.setScene(self.scene)
        self.view.setSceneRect(QtCore.QRectF(self.rect()))

        self.setCentralWidget(self.view)

        self.scene.addRect(0, 0, 420, 100, QtGui.QColor(QtCore.Qt.black), QtGui.QColor(QtCore.Qt.black))
        self.scene.addRect(500, 0, 524, 100, QtGui.QColor(QtCore.Qt.black), QtGui.QColor(QtCore.Qt.black))

        text_object = QtGui.QGraphicsTextItem("Amount: " + str(self.amount))
        text_object.setFont(QtGui.QFont("Times", 30))
        self.scene.addItem(text_object)
        text_object.setX(780.0)
        self.text_object_container.append(text_object)





class MyView(QtGui.QGraphicsView):

    def __init__(self):
        super(MyView, self).__init__()
        

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    exit_code = app.exec_()
    sys.exit(exit_code)