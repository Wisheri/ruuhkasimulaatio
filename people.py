import sys
import vector

def move(x, y, destination, x_collision, y_collision):
    location = vector.Vector2D(x, y)


    if x_collision and y_collision and (x > 430.0 and x < 480.0): #Prevents total blocking
        y_collision = False

    if x_collision and y_collision and (x < 420.0 or x > 475.0) and (y <= 130.0): #Prevents total blocking
        x_collision = False

    if x < destination[0] and x_collision == False:
        location += [1, 0]
    elif x > destination[0] and x_collision == False:
        location -= [1, 0]

    if y > destination[1] and y_collision == False:
        location -= [0, 1]
    elif y <= destination[1] and y_collision == False:
        return -1




    return location