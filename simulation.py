import sys
import random
import people
import time

from simulation_gui import *
from PyQt4 import QtGui, QtCore

class Simulation(object):

    def __init__(self):
        self.people = []
        self.simulation_stopped = False
        self.destroyed = 0
        self.destroy_list = []

    def move(self, destination):
        if len(self.people) == self.destroyed and self.destroyed != 0: #End of the simulation
            self.simulation_stopped = True

        coordinate_x = []
        coordinate_y = []

        counter = 0

        for person in self.people: #List of current locations
            position = person.scenePos()
            coordinate_x.append(position.x())
            coordinate_y.append(position.y())

        for person in self.people: #Updating locations
            position = person.scenePos()
            x_collision = False
            y_collision = False

            for l in range(0, len(coordinate_x)):
                i = coordinate_x[l]
                j = coordinate_y[l]
                if l != counter: #Checks that the object isn't handling its own coordinates

                    if (i <= position.x() and i > position.x() - 15.0): #Although the objects size if 10.0 I use 15.0 to keep some space between them
                        if j <= position.y() and j > position.y() - 15.0:
                            y_collision = True
                        if j >= position.y() and j < position.y() + 15.0:
                            x_collision = True
                    elif (i >= position.x() and i < position.x() + 15.0):
                        if j <= position.y() and j > position.y() - 15.0:
                            y_collision = True
                        if j >= position.y() and j < position.y() + 15.0:
                            x_collision = True

                    if position.y() <= 120.0 and (position.x() > 490.0 or position.x() < 430.0): #Objects don't hit the walls
                        y_collision = True
                if x_collision and y_collision: #If both values are true it isn't necessary to view the remaining coordinates
                    break
                if position.x() == 445 or position.x() == 455: #This spreads the objects nicely, otherwise they will be in a single line
                    x_collision = True
            counter += 1


            new_position = people.move(position.x(), position.y(), destination, x_collision, y_collision)
            if new_position == -1: #Object has reached it's destination
                person.setX(2000.0)
                person.setY(10.0)
                self.destroyed += 1
                self.destroy_list.append(person)
            else:
                person.setX(new_position[0])
                person.setY(new_position[1])
                

        
    def set_person(self, person):
        self.people.append(person)

    def get_people(self):
        return self.people

    def simulation_running(self):
        return self.simulation_stopped

    def clear_list(self):
        self.people = []
        self.simulation_stopped = False
        self.destroyed = 0

    def get_destroy_list(self):
        return self.destroy_list

    def list_destroyed(self):
        self.destroy_list = []

    def save_log(self, amount, end_time):
        localtime = time.asctime( time.localtime(time.time()) )

        f = open("log.txt", "a+")
        f.write(localtime + ": Amount " + str(amount) + " Time " + str(float(end_time) / 1000) + "\n")